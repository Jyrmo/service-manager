<?php
// Sample config
return array(
	'builders' => array(
		'literal' => 'Jyrmo\ServiceManager\ServiceBuilder\LiteralServiceBuilder',
		'invokable' => 'Jyrmo\ServiceManager\ServiceBuilder\InvokableServiceBuilder',
		'factory' => 'Jyrmo\ServiceManager\ServiceBuilder\FactoryServiceBuilder',
		'setter' => 'Jyrmo\ServiceManager\ServiceBuilder\SetterServiceBuilder'
		// Add your own builders here. Make sure they extend
		// Jyrmo\ServiceManager\ServiceBuilder\AbstractServiceBuilder
	),
	'services' => array(
		'literal' => array(),
		'invokable' => array(),
		'factory' => array(
			// Factories should implement Jyrmo\ServiceManager\ServiceBuilder\Factory\FactoryInterface
		),
		'setter' => array(
			'foo' => array(
				'className' => 'Foo',
				'properties' => array(
					'bar' => 'baz'
				),
			),
		),
	),
);