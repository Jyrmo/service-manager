<?php

namespace Jyrmo\ServiceManager;

use Jyrmo\ServiceManager\ServiceBuilder\ServiceBuilderManager;
use Jyrmo\ServiceManager\Exception\ServiceConfigFormatException;
use Jyrmo\ServiceManager\Exception\ServiceNotFoundException;
use Jyrmo\ServiceManager\Exception\ServiceTypeNotFoundException;

class ServiceManager implements ServiceManagerInterface {
	/**
	 * @var ServiceBuilder\ServiceBuilderManager
	 */
	protected $serviceBuilderManager;

	/**
	 * @var array
	 */
	protected $serviceConfig;

	/**
	 * @var array
	 */
	protected $mapServiceType;

	/**
	 * @var array
	 */
	protected $services = array();

	protected function buildMapServiceType() {
		$mapServiceType = array();
		foreach ($this->serviceConfig as $type => $specs) {
			foreach ($specs as $name => $spec) {
				$mapServiceType[$name] = $type;
			}
		}

		$this->mapServiceType = $mapServiceType;
	}

	protected function serviceIsInstantiated(string $name) : bool {
		$isInstantiated = isset($this->services[$name]);

		return $isInstantiated;
	}

	protected function instantiateService(string $name) {
		$this->services[$name] = $this->newInstance($name);
	}

	/**
	 * @throws ServiceNotFoundException
	 */
	protected function getServiceType(string $serviceName) : string {
		if (!isset($this->mapServiceType[$serviceName])) {
			throw new ServiceNotFoundException('No service with the name "' . $serviceName . '" was found.');
		}
		$serviceType = $this->mapServiceType[$serviceName];

		return $serviceType;
	}

	/**
	 * @throws ServiceTypeNotFoundException
	 * @throws ServiceNotFoundException
	 */
	protected function getServiceSpec(string $serviceName, $serviceType = null) {
		if (!$serviceType) {
			$serviceType = $this->getServiceType($serviceName);
		}
		if (!isset($this->serviceConfig[$serviceType])) {
			throw new ServiceTypeNotFoundException('Service type "' . $serviceType . '" not found in service config.');
		}
		if (!isset($this->serviceConfig[$serviceType][$serviceName])) {
			throw new ServiceNotFoundException('Service named "' . $serviceName . '" of type "' . $serviceType . '" not found in service config.');
		}
		$serviceSpec = $this->serviceConfig[$serviceType][$serviceName];

		return $serviceSpec;
	}

	/**
	 * @throws ServiceConfigFormatException
	 */
	public function __construct(array $config) {
		if (!isset($config['services'])) {
			throw new ServiceConfigFormatException('Key "services" not found in service config.');
		}
		$this->serviceConfig = $config['services'];

		if (!isset($config['builders'])) {
			throw new ServiceConfigFormatException('Key "builders" not found in service config.');
		}
		$this->serviceBuilderManager = new ServiceBuilderManager($config['builders'], $this);

		$this->buildMapServiceType();
	}

	public function newInstance(string $name) {
		$serviceType = $this->getServiceType($name);
		$serviceSpec = $this->getServiceSpec($name);
		$service = $this->serviceBuilderManager->buildService($serviceType, $serviceSpec);

		return $service;
	}

	public function get(string $name) {
		if (!$this->serviceIsInstantiated($name)) {
			$this->instantiateService($name);
		}

		return $this->services[$name];
	}
}
