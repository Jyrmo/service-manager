<?php

namespace Jyrmo\ServiceManager\Exception;

class ServiceTypeNotFoundException extends ServiceManagerException {}
