<?php

namespace Jyrmo\ServiceManager\Exception;

class ServiceNotFoundException extends ServiceManagerException {}