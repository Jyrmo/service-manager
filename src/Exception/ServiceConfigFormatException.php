<?php

namespace Jyrmo\ServiceManager\Exception;

class ServiceConfigFormatException extends ServiceManagerException {}
