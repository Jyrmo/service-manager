<?php

namespace Jyrmo\ServiceManager;

interface ServiceManagerInterface {
	public function newInstance(string $name);

	public function get(string $name);
}
