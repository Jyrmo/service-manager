<?php

namespace Jyrmo\ServiceManager\ServiceBuilder\Exception;

class SetterNotCallableException extends ServiceBuilderException {
}
