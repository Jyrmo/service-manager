<?php

namespace Jyrmo\ServiceManager\ServiceBuilder\Exception;

use Jyrmo\ServiceManager\Exception\ServiceManagerException;

class ServiceBuilderException extends ServiceManagerException {}
