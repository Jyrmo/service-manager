<?php

namespace Jyrmo\ServiceManager\ServiceBuilder\Exception;

class BuilderNotFoundException extends ServiceBuilderException {}
