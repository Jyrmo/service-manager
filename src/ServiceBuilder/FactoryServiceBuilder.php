<?php

namespace Jyrmo\ServiceManager\ServiceBuilder;

class FactoryServiceBuilder extends AbstractServiceBuilder {
	public function build($spec) {
		$factory = new $spec();
		$service = $factory->createService($this->serviceManager);
		
		return $service;
	}
}
