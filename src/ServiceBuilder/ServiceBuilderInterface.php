<?php

namespace Jyrmo\ServiceManager\ServiceBuilder;

interface ServiceBuilderInterface {
	public function build($spec);
}
