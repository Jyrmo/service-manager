<?php

namespace Jyrmo\ServiceManager\ServiceBuilder;

class InvokableServiceBuilder extends AbstractServiceBuilder {
	public function build($spec) {
		$service = new $spec();
		
		return $service;
	}
}
