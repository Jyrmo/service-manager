<?php

namespace Jyrmo\ServiceManager\ServiceBuilder\Factory;

use Jyrmo\ServiceManager\ServiceManagerInterface;

interface FactoryInterface {
	public function createService(ServiceManagerInterface $serviceManager);
}
