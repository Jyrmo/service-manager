<?php

namespace Jyrmo\ServiceManager\ServiceBuilder;

use Jyrmo\ServiceManager\Exception\ServiceConfigFormatException;
use Jyrmo\ServiceManager\ServiceBuilder\Exception\SetterNotCallableException;

class SetterServiceBuilder extends AbstractServiceBuilder {
	private function getSetterName(string $propName) : string {
		$capitalizedPropName = ucfirst($propName);
		$setterName = 'set' . $capitalizedPropName;

		return $setterName;
	}

	public function build($spec) {
		if (!isset($spec['className'])) {
			throw new ServiceConfigFormatException('Key "className" not found in service spec.');
		}

		$className = $spec['className'];
		$service = new $className();

		if (!isset($spec['properties'])) {
			throw new ServiceConfigFormatException('Key "properties" not found in service spec.');
		}
		if (!is_array($spec['properties'])) {
			throw new ServiceConfigFormatException('Spec["properties"] should refer to an array.');
		}
		foreach ($spec['properties'] as $propName => $serviceName) {
			$propVal = $this->serviceManager->get($serviceName);
			$setterName = $this->getSetterName($propName);
			if (!is_callable(array($service, $setterName))) {
				throw new SetterNotCallableException('Cannot call setter for property "' . $propName . '" on service "' . $serviceName . '".');
			}
			$service->$setterName($propVal);
		}

		return $service;
	}
}
