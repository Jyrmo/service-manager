<?php

namespace Jyrmo\ServiceManager\ServiceBuilder;

use Jyrmo\ServiceManager\ServiceManager;
use Jyrmo\ServiceManager\ServiceBuilder\Exception\BuilderNotFoundException;

class ServiceBuilderManager {
	/**
	 * @var array
	 */
	protected $config;

	/**
	 * @var \Jyrmo\ServiceManager\ServiceManager
	 */
	protected $serviceManager;

	/**
	 * @var array
	 */
	protected $builders = array();

	/**
	 * @throws BuilderNotFoundException
	 */
	protected function initBuilder(string $name) {
		if (!isset($this->config[$name])) {
			throw new BuilderNotFoundException('Builder class with the name "' . $name . '" not found in config.');
		}
		$builderClass = $this->config[$name];
		/**
		 * AbstractServiceBuilder
		 */
		$builder = new $builderClass();
		$builder->setServiceManager($this->serviceManager);
		$this->builders[$name] = $builder;
	}

	protected function getBuilder(string $name) : AbstractServiceBuilder {
		if (!isset($this->builders[$name])) {
			$this->initBuilder($name);
		}

		return $this->builders[$name];
	}

	public function __construct(array $config, ServiceManager $serviceManager) {
		$this->config = $config;
		$this->serviceManager = $serviceManager;
	}

	public function buildService(string $builderName, $serviceSpec) {
		$builder = $this->getBuilder($builderName);
		$service = $builder->build($serviceSpec);

		return $service;
	}
}
