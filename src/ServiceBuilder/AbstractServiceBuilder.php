<?php

namespace Jyrmo\ServiceManager\ServiceBuilder;

use Jyrmo\ServiceManager\ServiceManagerInterface;

abstract class AbstractServiceBuilder implements ServiceBuilderInterface {
	/**
	 * @var \Jyrmo\ServiceManager\ServiceManagerInterface
	 */
	protected $serviceManager;

	public function setServiceManager(ServiceManagerInterface $serviceManager) {
		$this->serviceManager = $serviceManager;
	}
}
